import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../product';
import { ProductServiceService } from '../product-service.service';
import { Cart } from '../cart';
import { User } from '../user';
import { UserService } from '../user.service'




@Component({
  selector: 'app-body-product-items',
  templateUrl: './body-product-items.component.html',
  styleUrls: ['./body-product-items.component.css']
})
export class BodyProductItemsComponent implements OnInit {

  product: Product[];
  user: User[];
  numberItems:number = 4;
  cart: Cart[] = [{
    quantity: 0
  },
  {
    quantity: 0
  },
  {
    quantity: 0
  },];

  total:number = 0;
  quantity:number =0;
  checkout:number = 0;
  isCheckoutOKe = false;

  constructor(private productService : ProductServiceService, private userInfo : UserService) { }
  getProduct(): void {
    this.productService.getProduct()
    .subscribe(product => {
      this.product = product;
    });
  }

  getUser(): void {
    this.userInfo.getUser()
    .subscribe(user => {
      this.user = user;
      console.log(this.user);
    });
  }




  countSum() {
    this.total = 0
    console.log(this.product)
    for(let i=0; i<this.product.length;i++){
      this.total += this.product[ i].quantity * this.product[i].price;
    }
    return this.total;
  }

  countQuantity(){
    this.quantity= 0
    console.log(this.product)
    for(let i=0; i<this.product.length;i++){
      this.quantity += this.product[ i].quantity;
    }
    return this.quantity;
  }

  countCheckout(){
    this.checkout= 0
    console.log(this.checkout)
    this.checkout = this.user[0].balance - this.total;
    // for(let i=0; i<this.checkout.length;i++){
    //   this.checkout = this.user[i].balance - this.total;
    // }
    if(this.checkout >=0){
      this.isCheckoutOKe = true;
      return this.checkout +  " USD";

    }
    else{
      this.isCheckoutOKe = false;
      return "Not Enough Money";
    }
  }


  ngOnInit(): void {
    // let square = this.numberItems * this.numberItems
    this.getProduct();
    this.getUser();
  }

  // removeProduct(productId:string):void{
  //   alert('Remove product' + ' ' + productId)
  //   const index = this.productList.findIndex(productList => productId === productId );
  //   this.productList.splice(index,1);
  // }

  // updateQuantity(element){
  //   console.log(element.value)
  //  // console.log(id.value);
  // }


}
