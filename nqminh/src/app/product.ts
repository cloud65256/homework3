export interface Product{
  id: string;
  title: string;
  price:number;
  type: string;
  image: string;
  quantity : number;
}
