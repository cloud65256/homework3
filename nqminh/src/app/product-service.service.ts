import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Product } from './product';

const PRODUCT_API = 'https://60498463fb5dcc001796a1fd.mockapi.io/api/v1/product';
@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {

  constructor(private http: HttpClient) { }

    getProduct() : Observable<Product[]>{
      return this.http.get<Product[]>(PRODUCT_API).pipe()
    }
}
