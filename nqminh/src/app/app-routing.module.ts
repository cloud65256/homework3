import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BodyComponent } from './body/body.component';
import { RouterModule, Routes } from '@angular/router';
import { BodyProductItemsComponent } from './body-product-items/body-product-items.component';



const routes: Routes = [
  { path: 'trangchu', component: BodyComponent },
  { path: 'muaban', component: BodyProductItemsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
