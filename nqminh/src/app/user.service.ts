import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { User } from './user';

const USER_API = 'https://60498463fb5dcc001796a1fd.mockapi.io/api/v1/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  getUser() : Observable<User[]>{
    return this.http.get<User[]>(USER_API).pipe()
  }
}
