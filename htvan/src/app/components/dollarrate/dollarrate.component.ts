import { Component, OnInit, ViewChild } from '@angular/core';
import { GoogleChartInterface } from 'ng2-google-charts';
import { interval } from 'rxjs';
import { GoldData } from 'src/app/models/gold-data';
import { GoldDataServiceService } from 'src/app/services/gold-data-service.service';

@Component({
  selector: 'app-dollarrate',
  templateUrl: './dollarrate.component.html',
  styleUrls: ['./dollarrate.component.css']
})
export class DollarrateComponent implements OnInit {

  @ViewChild('chart',{static: false}) chart;
  goldData: GoldData[] = [];
  currentBuying = 0.0;
  currentSelling = 0.0;
  currentAverage = 0.0;
  arrowType: String;
  lineChart: GoogleChartInterface = {
    chartType: 'LineChart'
  }
  showSpinner = true;
  constructor(private goldDataService: GoldDataServiceService) { }

  ngOnInit(): void {
    interval(1000).subscribe(x => {
      this.showSpinner = false;
      this.getGoldData();
      
    });  
  }

  getGoldData() {
  
    this.goldDataService.getGoldData().subscribe((result) => {
 
      let val: any = result;
        let gold: GoldData = {
        symbol: val.symbol,
        buy: +val.buy,
        sell: +val.sell,
        timestamp: new Date(val.timestamp)
      }

      console.log(gold);
      
      this.goldData.push(gold);
      this.updateChart();

      this.currentBuying = +(gold.buy).toFixed(2);
      this.currentSelling = +(gold.sell).toFixed(2);

      let newAverage = (this.currentBuying+this.currentSelling)/2;
      if (newAverage >= this.currentAverage) {
        this.arrowType = "fa-arrow-up";
      } else {
        this.arrowType = "fa-arrow-down";
      }
      this.currentAverage = newAverage;

     });
  }

  updateChart() {
    
    let dataTable = [];
    dataTable.push(['Date','Buy','Sell']);
    this.goldData.forEach(data => {
      dataTable.push([data.timestamp,data.buy,data.sell]);
    })
    this.lineChart = {
      chartType: 'LineChart',
      dataTable: dataTable,
      options: {
        height: 600
      },
    };
    this.chart.draw();
  }
}



