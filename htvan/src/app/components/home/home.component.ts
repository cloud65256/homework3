import { Component, OnInit } from '@angular/core';
declare var jQuery: any;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  newProductList = [
  {
    name: 'Silver bracelet',
    url: './assets/images/jewel1.jpg',
    productNumber: 'N123'
  },
  { 
    name: 'Golden bracelet',
    url: './assets/images/jewel2.jpg',
    productNumber: 'N1223'
  },
  {
    name: 'Blabla bracelet',
    url: './assets/images/jewel3.jpg',
    productNumber: 'N41273'
  },
  {
    name: 'Diamond bracelet',
    url: './assets/images/jewel4.jpg',
    productNumber: 'N1623'
  },
  {
    name: 'Bronze bracelet',
    url: './assets/images/jewel5.jpg',
    productNumber: 'N128388'
  },
  {
    name: 'Crystal bracelet',
    url: './assets/images/jewel6.jpg',
    productNumber: 'N12993'
  }
  ]

  constructor() { }

  ngOnInit(): void {
    (function ($) {
    $(document).ready(function(){

      $('.items').slick({
      dots: true,
      infinite: true,
      speed: 800,
      autoplay: true,
      autoplaySpeed: 2000,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
      {
      breakpoint: 1024,
      settings: {
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      dots: true
      }
      },
      {
      breakpoint: 600,
      settings: {
      slidesToShow: 2,
      slidesToScroll: 2
      }
      },
      {
      breakpoint: 480,
      settings: {
      slidesToShow: 1,
      slidesToScroll: 1
      }
      }
      
      ]
      });
  });
})(jQuery);
      
  }

  

}
