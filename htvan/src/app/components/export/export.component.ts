import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css']
})
export class ExportComponent implements OnInit {

  items = [
    {name: 'van', quantity: 1}, 
    {name: 'w', quantity: 2}
  ];
  sum = 0;
  constructor() { }

  ngOnInit(): void {
    
  }

count() {
  this.sum = 0;
  for(let i = 0; i < this.items.length; i++) {
    this.sum+=this.items[i].quantity;
  }
  return this.sum;
}
}
