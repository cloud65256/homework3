import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { GoldDataServiceService } from 'src/app/services/gold-data-service.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {

  opt: String;
  isRunning = false;
  alert = false;
  alert_apply = false;


  generateCondition = new FormGroup({
    initialBuying: new FormControl('54.0'),
    initialSelling: new FormControl('55.0'),
    amplitude: new FormControl('0.2')
  })
  filterCondition = new FormGroup({
    minBuying: new FormControl('0.0'),
    maxBuying: new FormControl('100.0'),
    minSelling: new FormControl('0.0'),
    maxSelling: new FormControl('100.0')
  })

  constructor(private goldDataService: GoldDataServiceService) { }

  ngOnInit(): void {
    this.opt = "option1";
  }
  
display(input: HTMLInputElement) {
  this.opt = input.value;
}

generate() {
 
  this.goldDataService.saveGenerateCondition(this.generateCondition.value).subscribe((result) => {
    this.alert = true;
    this.isRunning = !this.isRunning;
    this.generateCondition.reset({});
  })
}

applyFilter() {
  
}
clickStop() {
  this.isRunning = !this.isRunning;
  this.alert = false;
}
clickApply() {
  this.alert_apply = true;
}

closeAlert() {
  this.alert_apply = false;
}


}
