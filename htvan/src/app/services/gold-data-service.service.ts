import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GoldDataServiceService {
  private apiUrl = 'http://localhost:8080/goldratedata';

  getGoldData() {
    return this.http.get(this.apiUrl);
  }

  saveGenerateCondition(data) {
    return this.http.post(this.apiUrl, data);
  }


  constructor(private http: HttpClient) { }
}
