
export class User{
    fullname: string;
    email: string;
    phone: string;
    job: string;
    password: string;

    constructor(){
        this.fullname = '';
        this.email = '';
        this.phone = '';
        this.job = '';
        this.password = '';
    }
}
  