import { Component, OnInit } from '@angular/core';

interface Product {

  id: Number;

  imageUrl: String;

  name: String;

  code: String;

}

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent {
  name = 'Angular';

  productsA: Product[] = [
    {
      id: 1,
      imageUrl: './assets/image/body/new_product/day3-169.jpg',
      name: 'Vòng tay gắn đá Citrine',
      code: 'Mã sản phẩm: MH1-V'
    },
    {
      id: 2,
      imageUrl: './assets/image/body/new_product/IMG_4803.2.jpg',
      name: 'Nhẫn bạc gắn đá trắng tròn N170825011',
      code: 'Mã sản phẩm: N170825011'
    },
    {
      id: 3,
      imageUrl: './assets/image/body/new_product/nhan-vang-nu-ruby-hoa-ket-kim.jpg',
      name: 'Nhẫn vàn Ruby nữ kết kim hình hoa MH2-N171006054',
      code: 'Mã sản phẩm: MH2-N171006054'
    },
    {
      id: 4,
      imageUrl: './assets/image/body/new_product/lac_web_2.jpg',
      name: 'Nikon DSLR',
      code: 'Vòng tay mặt sao biển đá Emerald MH2-V'
    }
  ];

  productsB: Product[] = [
    {
      id: 1,
      imageUrl: './assets/image/body/new_product/nhan-vang-nu-ruby-hoa-ket-kim.jpg',
      name: 'Nhẫn vàn Ruby nữ kết kim hình hoa MH2-N171006054',
      code: 'Mã sản phẩm: MH2-N171006054'
    },
    {
      id: 2,
      imageUrl: './assets/image/body/new_product/lac_web_2.jpg',
      name: 'Nikon DSLR',
      code: 'Vòng tay mặt sao biển đá Emerald MH2-V'
    },
    {
      id: 3,
      imageUrl: './assets/image/body/new_product/lac bi bac meo kitty xanh.jpg',
      name: 'Lắc bi bạc trẻ em mèo Kitty xanh lá MH2-L180426013',
      code: 'Mã sản phẩm: MH2-L180426013'
    },
    {
      id: 4,
      imageUrl: './assets/image/body/new_product/vong bac meo kitty.jpg',
      name: 'Vòng bạc mèo Kitty cho bé MH2-V180402001',
      code: 'Mã sản phẩm: MH2-V180402001'
    }
  ]


}
