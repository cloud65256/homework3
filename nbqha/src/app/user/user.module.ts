import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BodyComponent } from './body/body.component';
import { RegisterComponent } from './register/register.component';

import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';


//Khai báo một constant chứa các route của app
const routes: Routes = [
  { path: '', component: BodyComponent },
  { path: 'register', component: RegisterComponent }
];

@NgModule({
  declarations: [HeaderComponent, FooterComponent, BodyComponent, RegisterComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  exports: [
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    RegisterComponent
  ]
})
export class UserModule { }
