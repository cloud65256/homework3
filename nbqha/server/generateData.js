var faker = require('faker');

var database = { products: []};

for (var i = 1; i<= 300; i++) {
  database.products.push({
    id: i,
    imageUrl: "https://source.unsplash.com/1600x900/?product",
    name: faker.commerce.productName(),
    quantity: faker.random.number()
  });
}

console.log(JSON.stringify(database));