export interface User{
  id: string;
  wallet: number;
  amount: number;
}
