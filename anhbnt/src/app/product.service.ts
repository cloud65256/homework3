import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable,of} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Product } from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
private goldUrl = 'https://60482d73b801a40017ccd5cb.mockapi.io/api/v1/Gold';
  constructor(private http: HttpClient) { }
//
// getGold(id : string): Observable<Product> {
// const url = `${this.goldUrl}/${id}`;
//     return this.http.get<Product>(url).pipe();
//
// }

  getGold(): Observable<Product[]> {
    return this.http.get<Product[]>(this.goldUrl)
      .pipe(
      );
  }
}
