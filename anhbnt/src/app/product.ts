export interface Product{
  id: string;
  price : number;
  quantity : number;
}
