export interface Cart{
  id: string;
  quantity: number;
  total: number;
}
