import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable,of} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {



  private userUrl = 'https://60482d73b801a40017ccd5cb.mockapi.io/api/v1/user';
    constructor(private http: HttpClient) { }

    getUser(): Observable<User[]> {
      return this.http.get<User[]>(this.userUrl)
        .pipe(
        );
    }
}
