import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ContentComponent } from './content/content.component';
import { AppRoutingModule } from './app-routing.module';
import { ExchangeRateComponent } from './content/exchange-rate/exchange-rate.component';
import { TransactionComponent } from './content/transaction/transaction.component';
import { ExportComponent } from './content/export/export.component';
import { SignupComponent } from './content/signup/signup.component';
import { AdminPageComponent } from './content/admin-page/admin-page.component';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ContentComponent,
    ExchangeRateComponent,
    TransactionComponent,
    ExportComponent,
    SignupComponent,
    AdminPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
