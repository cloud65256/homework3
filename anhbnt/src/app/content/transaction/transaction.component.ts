import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../../product.service';
import { Product } from '../../product';
import { UserService } from '../../user.service';
import { User } from '../../user';
import { Cart } from '../../cart';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {
  gold : Product[];
  gold1 : Product;
  user: User[];
  user1: User;

  cart : Cart = {
    id:'',
    quantity:0,
    total:0
  }

  constructor(private productService : ProductService, private userService : UserService ) { }

  ngOnInit(): void {
    this.getGold();
    this.getUser();
  }
  getGold() : void {
    this.productService.getGold()
    .subscribe(product => {this.gold = product;
      this.gold1 = this.gold[0];
    });

    }

    getUser() : void {
      this.userService.getUser()
      .subscribe(user => {this.user = user;
        this.user1 = this.user[0];
      });
      }


  calculate(){
    if ((this.user1.wallet-((this.gold1.price*this.cart.quantity) + (this.gold1.price*this.cart.quantity*10/100))) < 0)
    {
      return alert ('Giao dịch thất bại do không đủ tiền trong ví');
    }
    else if ( this.cart.quantity == 0)
    {
      return alert ('Giao dịch thất bại do không có sản phẩm nào');
    }
    else
    {
      return alert('Giao dịch thành công');
    }
    }

}
